# 勤怠押す太郎
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import configparser
import os
import datetime

inifile = configparser.ConfigParser()
path = os.getcwd()
# ログイン用設定ファイルの読込
inifile.read(path+'\\kintai.ini', 'UTF-8')

# Chrome起動
options = webdriver.ChromeOptions()
options.add_argument('--headless')                  # ヘッドレスモード
options.add_argument('--disable-gpu')               # 昔必要だったらしい引数
options.add_argument('--ignore-certificate-errors') # SSLエラー無視
# chromedriverのバージョンを指定
driver = webdriver.Chrome(options=options, executable_path='chromedriverを置いた場所\\chromedriver.exe')

# 勤怠へアクセス
driver.get('勤怠サービスのURL')
print(driver.title)
# フレームを取得して切替
frame=driver.find_elements_by_xpath("//frame")[1]
driver.switch_to.frame(frame)
time.sleep(1)

driver.find_element_by_name('DataSource').send_keys(inifile.get('user_info', 'company'))
driver.find_element_by_name('LoginID').send_keys(inifile.get('user_info', 'id'))
driver.find_element_by_name('PassWord').send_keys(inifile.get('user_info', 'pass'))
driver.find_element_by_name('btnLogin').click()

time.sleep(1)
driver.find_element_by_name('IN').click()
driver.find_element_by_name('punch').click()
time.sleep(1)
driver.save_screenshot(str(datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S")) + '.png')
driver.find_element_by_name('OK').click()
driver.quit()